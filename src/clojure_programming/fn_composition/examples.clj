(ns clojure-programming.fn-composition.examples)

(/ (apply + 50 (range 10)) 2)
(->>
  (range 10)
  (apply + 50)
  (* 1/2)
  )


(defn negated-sum-str
  "doc string"
  [& numbers]
  (str (- (apply + numbers)))
  )

(def negated-sum-str-comp (comp str - +))


(defn negated-sum-str-macro
  "doc string"
  [& coll]
  (->>
    (apply + coll)
    -
    str)
  )

(negated-sum-str 1 2 3 4 5)
(negated-sum-str-comp 1 2 3 4 5)
(negated-sum-str-macro 1 2 3 4 5)
(negated-sum-str-macro2 1 2 3 4 5)

(def camel->keyword 
  (comp keyword
        clojure.string/join
        (partial interpose \-)
        (partial map clojure.string/lower-case)
        #(clojure.string/split % #"(?<=[a-z])(?=[A-Z])"))
    )

(defn camel->keyword 
  [s]
  (->> 
    (clojure.string/split s #"(?<=[a-z])(?=[A-Z])")
    (map clojure.string/lower-case)
    (interpose \-)
    clojure.string/join
    keyword)
  )

(camel->keyword "CamelCase")
(camel->keyword "AllThingsAreCrazy")
