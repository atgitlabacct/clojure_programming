(ns clojure-programming.collections-and-structures.examples)

(def orders
  [{:product "Clock" :customer "Wile Coyote" :qty 6 :total 300}
   {:product "Dynamite" :customer "Wile Coyote" :qty 20 :total 5000}
   {:product "Shotgun" :customer "Elmer Fudd" :qty 2 :total 800}
   {:product "Shells" :customer "Elmer Fudd" :qty 4 :total 100}
   {:product "Hole" :customer "Wile Coyote" :qty 1 :total 1000}
   {:product "Anvil" :customer "Elmer Fudd" :qty 2 :total 300}
   {:product "Anvil" :customer "Wile Coyote" :qty 6 :total 900}
   ])

(defn reduce-by
  "doc string"
  [key-fn f init coll]
  (reduce (fn [summaries x]
            (let [k (key-fn x)]
              (assoc summaries k (f (summaries k init)  x))))
          {}, coll)
  )

(reduce-by :customer #(+ %1 (:total %2)) 0 orders)

(reduce-by :product #(conj %1 (:customer %2)) #{} orders)
