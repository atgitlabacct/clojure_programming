(ns clojure-programming.macros.examples)

;; Unhygenic macro
;; This is beause on the let we are binding x to ourside
;; and x is bound in the macro to inside -- confusion, bug, painful
(defmacro print-marcro-unhygenic
  [& body]
  "Macro that is not clean" 
  `(let [~'x :inside]
     ~@body)
  )

(let [x :outside]
  (print-marcro-unhygenic 
    (println "x is " x))
  )

;; Hygenic macro
(defmacro print-mac
  "Prints the passed in value
  This is also a hygenic function since we you autogen (x#))
  "
  [& body]
  `(let [x# :macro-val]
     ;; (println ~x) ;; you can't do this because x is an autgened symbol
     (println ~@body)
     )
  )

(print-mac "Adam")

(macroexpand '(print-mac "Adam"))

(defmacro auto-gensyms
  [& numbers]
  "" 
  `(let [x# (rand-int 10)]
    (+ x# ~@numbers))
  )

(macroexpand '(auto-gensyms 1 2 3 4 5))
