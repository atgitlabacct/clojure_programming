(ns clojure-programming.concurrency.delays
  (:require [clojure.pprint :refer [pprint]]))

;; Clojure gives us a variety of ways to control how and when computation
;; is executed instead of the often buggy code of threads and locks.
;; The way clojure does this is limit the amount of mutable state by using
;; immutable data values and collections.  However they do give us options to
;; mutate state by reference types which have constraints around them in a 
;; cuncurrent thread environment.

;; Delays
;; Delays allow us to suspend a piece of code until it is needed.
(def my-delay
  (delay
    (println "Waking from cryo-sleep!")))

;; In order to use my-delay we have to deref the var.  You can also use the
;; '@' reader macro to deref 
@my-delay
(deref my-delay)

;; You can also do this by just using functions.  Just pass an anonymous
;; function back when called.
(def a-fn
  (fn [] "waking from cryp-sleep!"))
(a-fn)

;; Delays have a few benefits
;; One is that the body of code only runs once and caches the return value.
;; So if you deref a second time the code doesn't run and it returns instantly.
;; In a multithreaded app you can safely deref a delay and all threads will
;; block.  However after it is evaluated the result is no cached an each thread
;; has instant access to the result.

(println "Function with metadata and delays")
(defn calc-ship-rate
  []
  {:url "http://www.someshippingcompany.com"
   :title "Shipping calculator"
   :mime/type "text/html"}
  "some expensive calculation")

(def ltl-calc
  (delay
    (calc-ship-rate)))
ltl-calc

;; We can also see if a delay has been realized (fancy name for evaluated).
;; So if our application for whatever reason only wanted the value of ltl-calc
;; if it had already been executed we could use 'realized?'
(realized? ltl-calc)
