(ns clojure-programming.list-comp.examples)

(for [numbers [1 2 3]
      letters [:a :b :c :d]]
  (str numbers letters))

(mapcat
  (fn [number] 
    (map 
      (fn [letter] (str number letter)) [:a :b :c :d])) [1 2 3])

(for [tumbler1 (range 10)
      tumbler2 (range 10)
      tumbler3 (range 10)
      :when (or (= tumbler1 4)
                (= tumbler2 4)
                (= tumbler3 4))]
  [tumbler1 tumbler2 tumbler3])

(def capital-letters (map char (range (int \A) (inc (int \Z)))))
(def black-listed #{\I \O})

(for [letter-1 capital-letters
      letter-2 capital-letters
      :when (and (not (black-listed letter-1))
                 (not (black-listed letter-2)))]
  (str letter-1 letter-2))


(for [number [1 3 4 5]
      :let [tripled (* number 3)]
      :while (odd? tripled)]
  tripled)

(defn palindrome?
  "determines if number is a palindrome"
  [number]
  (= (str number) (clojure.string/reverse (str number))))

(= true
   (palindrome? 101))

(apply max (for [three-digit-number-1 (range 100 1000)
                 three-digit-number-2 (range 100 1000)
                 :let [product (* three-digit-number-1 three-digit-number-2)]
                 :when (palindrome? product)]
             product))
